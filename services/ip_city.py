"""
Unverified email: 0req/day - $0/mo
Free: 100req/day - $0/mo
Basic: 2'000req/day - $5/mo
Deluxe: 10'000req/day - $20/mo
Pro: 50'000req/day - $80/mo
Ultimate: 100'000req/day - $150/mo
Platinum: 500'000req/day - $500/mo
"""

import requests


API_KEY = "1ee458ad36f47248448675bab48a34db"
URL = f"https://ip.city/api.php?ip=%s&key={API_KEY}"
PRICE = "$80 /1500k"


def run(ip):
    try:
        response = requests.request("GET", URL % ip, headers={}, data={})
        data = response.json()
        return f"{data['countryCode']}-{data['city']}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
