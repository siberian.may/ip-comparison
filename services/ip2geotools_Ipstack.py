"""
free - 100 /month
$9.99 - 50'000 /month
$49.99 - 500'000 /month
$99.99 - 2'000'000 /month
"""

from ip2geotools.databases.noncommercial import Ipstack

PRICE = "$99.99 /2000k"


def run(ip):
    try:
        response = Ipstack.get(ip, api_key="961df1b423e3ca68d8390159aafead28")
        return f"{response.country}-{response.city}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
