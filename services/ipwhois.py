"""
free - 1'000 /day
$12 - 60'000 /month
$20 - 150'000 /month
$39 - 500'000 /month
$79 - 2'000'000 /month
$159 - 6'000'000 /month
$319 - 15'000'000 /month
"""

import json
from urllib.request import urlopen


URL = f"http://ipwho.is/%s"
PRICE = "$79 /2000k"


def run(ip):
    try:
        response = urlopen(URL % ip)
        data = json.load(response)
        return f"{data['country']}-{data['city']}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
