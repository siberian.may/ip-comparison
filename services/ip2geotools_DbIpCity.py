"""
free
$8.29 - 50'000 /day
$99.90 - 1'000'000 /day
"""

from ip2geotools.databases.noncommercial import DbIpCity

PRICE = "$8.29 /1500k"


def run(ip):
    try:
        response = DbIpCity.get(ip, api_key="free")
        return f"{response.country}-{response.city}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
