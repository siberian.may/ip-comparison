"""
free - 30'000 /month
$15 - 150'000 /month
$65 - 1'000'000 /month
$130 - 3'000'000 /month
$200 - 6'000'000 /month
$500 - 20'000'000 /month

"""

import json
from urllib.request import urlopen

API_KEY = "e898e3a4f0294107bb77e995f80d346f"
URL = f"https://api.ipgeolocation.io/ipgeo?apiKey={API_KEY}&ip=%s"
PRICE = "$65 /1000k"


def run(ip):
    try:
        response = urlopen(URL % ip)
        data = json.load(response)
        return f"{data['country_code2']}-{data['city']}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
