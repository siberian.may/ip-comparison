# Doesn't response

import json
from urllib.request import urlopen


URL = f"https://ipapi.co/%s/json/"


def run(ip):

    with urlopen(URL % ip) as url:
        data = url.read().decode()
        data = data.split("(")[1].strip(")")
    print(data)


if __name__ == "__main__":
    print(run("178.184.76.252"))
