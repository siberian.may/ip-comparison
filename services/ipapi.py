"""
$10.99 - 250'000 /month
$39.99 - 1'500'000 /month
$99.99 - 8'000'000 /month
------------
Yearly - 2 months free
"""

import json
from urllib.request import urlopen


URL = f"https://ipapi.co/%s/json/"
PRICE = "$40 /1500k"


def run(ip):
    try:
        response = urlopen(URL % ip)
        data = json.load(response)
        return f"{data['country']}-{data['city']}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
