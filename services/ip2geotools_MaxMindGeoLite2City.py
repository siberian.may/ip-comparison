"""
free
Registration and DB required
"""

from ip2geotools.databases.noncommercial import MaxMindGeoLite2City

PRICE = "FREE"


def run(ip):
    try:
        response = MaxMindGeoLite2City.get(ip, api_key="free")
        return f"{response.country}-{response.city}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
