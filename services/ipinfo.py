"""
free - 50'000 /month
$99 - 150'000 /month
$249 - 250'000 /month
$499 - 500'000 /month
"""

import json
from urllib.request import urlopen


TOKEN = "06dfa02ee681f8"
URL = f"https://ipinfo.io/%s\?token={TOKEN}"
PRICE = "$499 /500k"


def run(ip):
    try:
        response = urlopen(URL % ip)
        data = json.load(response)
        return f"{data['country']}-{data['city']}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
