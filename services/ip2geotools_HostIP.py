"""
free
"""

from ip2geotools.databases.noncommercial import HostIP

PRICE = "FREE"


def run(ip):
    try:
        response = HostIP.get(ip)
        return f"{response.country}-{response.city}".replace(",", ".")
    except:
        return "-"


if __name__ == "__main__":
    print(run("178.184.76.252"))
