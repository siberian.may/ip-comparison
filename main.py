import datetime
from time import sleep

from services import (
    ip2geotools_DbIpCity,
    ip2geotools_HostIP,
    ip2geotools_Ipstack,
    ip2geotools_MaxMindGeoLite2City,
    ip_city,
    ipapi,
    ipgeolocation,
    ipinfo,
    ipwhois,
)

DEBUG = False
IPS_FILE = "ips.txt" if not DEBUG else "ips_test.txt"
OUT_FILE = "result.csv"


SERVICES = {
    "ip2geotools_DbIpCity": ip2geotools_DbIpCity,
    "ip2geotools_HostIP": ip2geotools_HostIP,
    "ip2geotools_Ipstack": ip2geotools_Ipstack,
    "ip2geotools_MaxMindGeoLite2City": ip2geotools_MaxMindGeoLite2City,
    "ip_city": ip_city,
    "ipapi": ipapi,
    "ipgeolocation": ipgeolocation,
    "ipinfo": ipinfo,
    "ipwhois": ipwhois,
}


def read_ips():
    with open(IPS_FILE, "r") as ips_file:
        ips = ips_file.readlines()
    return ips


def get_time_left(start_time, number, total_number):
    time_delta = datetime.datetime.now() - start_time
    return time_delta * (total_number - number) / number


def parse_ips(ip_addresses):
    with open(OUT_FILE, "w+", encoding="utf-8") as out_file:
        # Titles line
        out_file.write("IP,EXPECTED," + ",".join(SERVICES.keys()) + "\n")
        # Prices line
        out_file.write(",," + ",".join([lib.PRICE for lib in SERVICES.values()]) + "\n")

        start_time = datetime.datetime.now()
        for i, ip_line in enumerate(ip_addresses):
            num = i + 1

            ip, expected_city = ip_line.strip().split(",")
            res = [lib_func.run(ip) for lib_func in SERVICES.values()]
            out_file.write(f"{ip},{expected_city}," + ",".join(res) + "\n")

            sleep(2)
            time_left = get_time_left(start_time, num, len(ip_addresses))
            print(f"\r{num}/{len(ip_addresses)} | {time_left} left", end="")


if __name__ == "__main__":
    ips = read_ips()
    parse_ips(ips)
